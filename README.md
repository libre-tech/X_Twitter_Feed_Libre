# BRC20 Social

Gitlab Repository: https://gitlab.com/libre-tech/BRC20_Social

Public JSON Data of Twitter Feed:  https://gitlab.com/libre-tech/BRC20_Social/index.json"

# Add Tweets

Want to add Tweets to this list for a BRC20?

Fork this repo and submit a PR with a an array of Tweet(X) IDs added to append to the index.json here in this repo.

## Steps to Add Posts for any BRC20: 

1. Gitlab - Login (or create account)
2. Open up - https://gitlab.com/libre-tech/BRC20_Social/
3. Click index.json
4. Click “Edit” - from dropdown select “Single File Only” 
5. Click “Fork” button (only need to do this the first time) 
6. Make changes to file - add tweets, remove, change order, whatever you want
7. Scroll down and click “Commit Changes”
8. Click “Create Merge Request” 
9. Wait for approval and merge from maintainer of repository
10. Changes go live once approved

## Option 2 - use the python scripts in this repo:

Here is a tweet - let's say I want to make this show for the token "BRC1":
https://twitter.com/bensig/status/1687146117396406284


This can be done by typing:

`python add_tweet BRC1 https://twitter.com/bensig/status/1687146117396406284`

or 

`python add_tweet BRC1 1687146117396406284`

Here is the data that will be added to the bottom of index.json:

```
  "BRC1": [
    "1687146117396406284"
  ]
```

We will review and merge these PRs. Thanks!
